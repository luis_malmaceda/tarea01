package com.example.firstapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnProcesar.setOnClickListener {
            val mayoriaDeEdad : Int = 18
            val edadIngresa1 : String = edtEdad.text.toString();
            if(edadIngresa1==""){
                edtEdad.requestFocus()
                tvResultado.text = ""
                return@setOnClickListener
            }
            val edadIngresada : Int =  edadIngresa1.toInt()

            if(edadIngresada>=mayoriaDeEdad){
                tvResultado.text = "Usted es mayor de edad!"
            }else{
                tvResultado.text = "Usted es menor de edad!"
            }
        }

    }
}